const express = require('express');
const router = express.Router();

const carsRoute = require('./cars.route');


// Cars
router.use('/cars',carsRoute);

module.exports = router;